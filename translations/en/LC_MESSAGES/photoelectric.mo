��          �                         &     C     Y     n  	   }     �     �     �     �     �     �     �     �  �  �     �     �     �     �  '   �               2     B     N     _     x     �     �   Photocurrent vs. Voltage Photocurrent vs. Wave Length Photoelectric Current Photoelectric Effect Series from-to Start [V] Step [V] Stop [V] Voltage [V] Wave Length [nm] WaveLnStep [nm] WaveLnStop [nm] WaveLnVoltage [V] WaweLnStart [nm] Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-08-01 19:27+0200
PO-Revision-Date: 2018-08-01 18:33+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Photocurrent vs. Voltage Photocurrent vs. Wave Length Photocurrent [nA] Photoelectric Effect Series from {from} to {to}, step {step} Start Voltage [V] Step of Voltage [V] End Voltage [V] Voltage [V] Wave Length [nm] Step of Wave Length [nm] End Wave Length [nm] Polarization Voltage [V] Start Wave Length [nm] 