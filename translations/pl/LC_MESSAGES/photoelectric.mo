��          �                         &     C     Y     n  	   }     �     �     �     �     �     �     �     �  �  �     �     �     �       $     %   B  "   h  "   �     �     �     �     �           '   Photocurrent vs. Voltage Photocurrent vs. Wave Length Photoelectric Current Photoelectric Effect Series from-to Start [V] Step [V] Stop [V] Voltage [V] Wave Length [nm] WaveLnStep [nm] WaveLnStop [nm] WaveLnVoltage [V] WaweLnStart [nm] Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-08-01 19:27+0200
PO-Revision-Date: 2018-08-01 18:33+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: pl
Language-Team: pl <LL@li.org>
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Photocurrent vs. Voltage Fotoprąd vs. długość fali Fotoprąd [nA] Efekt fotoelektryczny Seria od {from} do {to}, krok {step} Początkowe napięcie polaryzacji [V] Przyrost napięcia polaryzacji [V] Końcowe napięcie polaryzacji [V] Napięcie [V] Długość fali [nm] Przyrost długości fali [nm] Końcowa długość fali [nm] Napięcie polaryzacji [V] Początkowa długość fali [nm] 