from silf.backend.drivers.motech_lps304.motech_lps304_device import LPS304Device
from silf.backend.drivers.multimeter_hioki.hioki_3238_device import Hioki3238Device
from silf.backend.drivers.triax.triax_190_device import Triax190Device

from silf.backend.commons.simple_server.simple_server import ExperimentMode, ResultManager
from silf.backend.commons.api.stanza_content import ControlSuite, NumberControl, \
    OutputFieldSuite, ChartField
from silf.backend.commons.device_manager._result_creator import XYChartResultCreator

from silf.backend.commons.util.math_utils import round_sig
from silf.backend.commons.api.translations.trans import _
import logging
from time import time
from enum import Enum

class PhotoExperiment(ExperimentMode):
    name = 'photoelectric'
    label = _('Photoelectric Effect')
    chart_label = ''

    class States(Enum):
        SET = 0
        WAIT = 1
        READ = 3

    state = States.SET
    time_tmp=0

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)

        """Devices"""
        self.triax = None
        self.motech = None
        self.hioki = None
        """
        Two type of devices:
        control: device which controls the experiment,
                 in WaveLength mode this is the Triax monochromator,
                 in Voltage mode - the Motech supply
        parameter: the device which control the parameter of experiment,
                 parameter does not change for given time serie,
                 in WaveLength mode this is the the Motech supply
                 in Voltage mode - the Triax monochromator
        """
        self.__hioki_init_com = [
                ':TRIG:SOUR IMM',
                ':INIT:CONT 1',
                ':SAMP:RATE MED',
                'FUNC VOLT:DC',
                'VOLT:RANG:AUTO 1',
                ':CALC:AVER:STAT 1',
                ':CALC:AVER 5'
        ]
        # Monochromator slit size
        self.slit = 1200
        # value of resistor to calculate current
        self.resistor = 46000

        self.__started = False
        self.__triax_started = False
        self.point = 0  # Nuber of current point
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[XYChartResultCreator("chart", read_from=["control_value", "current"]), ])
        self.logger = logging.getLogger('photo')

    @classmethod
    def get_description(cls) -> str:
        return cls.label

    @classmethod
    def get_mode_label(cls) -> str:
        return cls.label

    @classmethod
    def get_mode_name(cls) -> str:
        return cls.name

    @classmethod
    def get_output_fields(cls) -> OutputFieldSuite:
        return OutputFieldSuite(
            chart=ChartField(
                html_class="chart", name=["chart"],
                label=cls.label,
                axis_x_label=cls.chart_label, axis_y_label=_('Photoelectric Current')
            ),
        )

    @classmethod
    def get_series_name(cls, settings: dict) -> str:
        return _("Series from-to").format(**settings)

    def power_up(self):
        self.logger.debug('PowerUp')
        self.triax = Triax190Device()
        self.triax.init(self.config)
        #self.triax.power_up()
        self.motech = LPS304Device()
        self.motech.init(self.config)
        self.motech.power_up()
        self.hioki = Hioki3238Device()
        self.hioki.init(self.config)
        self.hioki.power_up()

    def power_down(self):
        self.logger.debug('PowerDown')
        if self.__triax_started:
            self.triax.power_down()
        self.__triax_started = False
        self.triax = None
        self.motech.power_down()
        self.motech = None
        self.hioki.power_down()
        self.hioki = None

    def start(self):
        """
        Override it
        """
        pass

    def _start(self):
        self.logger.debug('Start')
        self.motech.start_measurements()
        self.hioki.start_measurements(self.__hioki_init_com)
        self.__started = True
        self.time_tmp=time()

    def stop(self):
        self.logger.debug('Stop')
        self.motech.stop_measurements()
        self.__started = False
        self.point = 0
        self.result_manager.clear()

    def loop_iteration(self):
        if self.__started:
            if not self.__triax_started:
                self.logger.debug('TriaxInit')
                #power_up triax - it takes time so it is not done in power up
                self.triax.power_up()
                self.triax.start_measurements(self.slit)
                self.__triax_started = True
            self._set_state()
            self._wait(2)
            self._read_state()

    def set_state(self):
        """
        Override it
        """
        pass

    def _set_state(self):
        if self.state == self.States.SET:
            setting = self.settings['from'] + self.point * self.settings['step']
            if  setting > self.settings['to']:
                self.experiment_callback.send_series_done()
                self.__started = False
                self.stop()
                self.point = 0
            else:
                self.set_state(setting)
                self.point += 1
                self.state = self.States.WAIT


    def _wait(self, t_wait):
        if self.state == self.States.WAIT:
            t = time()
            if t - self.time_tmp > t_wait:
                self.time_tmp = t
                self.state = self.States.READ

    def read_state(self):
        """
        Override it
        """
        pass
    def _read_state(self):
        if self.state == self.States.READ:
            res_control, res_current = self.read_state()
            self.logger.debug(res_control)
            self.logger.debug(res_current)
            self.result_manager.push_results({
                'control_value': res_control,
                'current': round_sig(1000000000*res_current/self.resistor, 3)})
            self.state = self.States.SET

class WaveLnMode(PhotoExperiment):
    name = "waveln"
    label = _("Photocurrent vs. Wave Length")
    chart_label = _("Wave Length [nm]")

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl(
                "from", _("WaweLnStart [nm]"), min_value=300, max_value=600, step=1, default_value=400
            ),
            NumberControl(
                "to", _("WaveLnStop [nm]"), min_value=610, max_value=1200, step=1, default_value=1200
            ),
            NumberControl(
                "step", _("WaveLnStep [nm]"), min_value=1, max_value=50, step=1, default_value=20
            ),
            NumberControl(
                "param", _("WaveLnVoltage [V]"), min_value=-5, max_value=20, step=0.1,
                default_value=8
            ),

        )

    def start(self):
        self._start()
        self.motech.set_v(self.settings['param']+5)

    def set_state(self, wave):
        self.triax.set_waveln(wave)

    def read_state(self):
        return int(self.triax.get_waveln()), self.hioki.read_state()

class VoltageMode(PhotoExperiment):
    name = "voltage"
    label = _("Photocurrent vs. Voltage")
    chart_label = _("Voltage [V]")

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl(
                "from", _("Start [V]"), min_value=-5, max_value=0, step=0.1, default_value=-5
            ),
            NumberControl(
                "to", _("Stop [V]"), min_value=0, max_value=20, step=0.1, default_value=7
            ),
            NumberControl(
                "step", _("Step [V]"), min_value=0.01, max_value=1, step=0.01, default_value=0.25
            ),
            NumberControl(
                "param", _("Wave Length [nm]"), min_value=300, max_value=1200, step=1, default_value=600
            ),

        )

    def start(self):
        self._start()

    def set_state(self, voltage):
        self.triax.set_waveln(self.settings['param'])
        self.motech.set_v(voltage+5)

    def read_state(self):
        return round_sig(self.motech.get_v()-5, 2), self.hioki.read_state()
