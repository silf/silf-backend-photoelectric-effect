import configparser
from silf.backend.commons.simple_server.simple_server import MultiModeManager
from .modes import WaveLnMode, VoltageMode

class PhotoExperiment(MultiModeManager):

    @classmethod
    def get_mode_managers(
        cls, config_parser: configparser.ConfigParser
    ):
        return [
            WaveLnMode,
            VoltageMode
        ]

    def post_power_up_diagnostics(self, *args, **kwargs):
        pass

    def pre_power_up_diagnostics(self, *args, **kwargs):
        pass

